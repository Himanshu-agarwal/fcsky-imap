package com.franconnect;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

import com.sun.mail.imap.IMAPSSLStore;
import com.sun.mail.imap.IMAPStore;

public class IMAPPOC {

	private static String password = "";

	private static String email = "";

	private static String oauthToken = "";

	private static boolean isPassword = true;

	public static void main(String[] args) {
		try {
			Store store = null;
			if (isPassword) {
				Properties props = new Properties();
				props.put("mail.store.protocol", "imaps");
				Session session = Session.getDefaultInstance(props, null);
				store = session.getStore("imaps");
				store.connect("imap.gmail.com", email, password);
			}
			else {
				store = connectToImap("imap.gmail.com", 993, email, oauthToken, true);
			}
			
			// if you want mail from specified folder, just change change folder name
			// Folder inbox = store.getFolder("[Gmail]/Drafts");
			Folder inbox = store.getFolder("inbox");

			inbox.open(Folder.READ_ONLY);
			int messageCount = inbox.getMessageCount();
			javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
			int i = 1;
			for (javax.mail.Folder folder : folders) {
				if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
					System.out.println("Folder_" + i + ": " + folder.getName());
					i++;
				}
			}

			System.out.println("Total MessagesCount :- " + messageCount);
			javax.mail.Message[] messages = inbox.getMessages();

			System.out.println("------------------------------");
			for (int j = 0; i < messages.length; j++) {
				System.out.println("Mail Subject:" + ": " + messages[j].getSubject());
			}
			inbox.close(true);
			store.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static IMAPStore connectToImap(String host, int port, String userEmail, String oauthToken, boolean debug)
			throws Exception {
		Properties props = new Properties();
		props.put("mail.imaps.sasl.enable", "true");
		props.put("mail.imaps.sasl.mechanisms", "XOAUTH2");
		props.put(OAuth2SaslClientFactory.OAUTH_TOKEN_PROP, oauthToken);
		Session session = Session.getInstance(props);
		session.setDebug(debug);

		final URLName unusedUrlName = null;
		IMAPSSLStore store = new IMAPSSLStore(session, unusedUrlName);
		final String emptyPassword = "";
		store.connect(host, port, userEmail, emptyPassword);
		return store;
	}

}
